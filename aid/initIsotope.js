
var iso = new Isotope('#banners_container', {
    // options
    /*layoutMode: 'packery',
    packery: {
        gutter: 10
    },*/
    layoutMode: 'vertical',
    vertical: {
        horizontalAlignment: 0.5,
    },
    itemSelector: '.banner',
});

function onResize() {

    //if (window.innerWidth > 728) {
    iso.arrange({
        layoutMode: 'packery',
        packery: {
            gutter: 10
        }
    });
    /*}
    else {
        iso.arrange({
            layoutMode: 'vertical',
            vertical: {
                horizontalAlignment: 0.5,
            }
        });
    }*/

    /*var banners = document.getElementsByClassName('banner');
    for (var i = 0; i < banners.length; i++) {
        var banner = banners[i];
        if (banner.offsetWidth > window.innerWidth) {
            banner.className += ' hidden';
        }
        else {
            banner.className = 'banner';
        }
    }*/

    iso.arrange({
        // item element provided as argument
        filter: function (banners) {
            var className = banners.className;
            return className.indexOf('hidden') < 0;
        }
    });
}

window.onresize = function () {
    onResize();
}

//RELEASE THE KRAKEEEEEEN!!!
onResize();
