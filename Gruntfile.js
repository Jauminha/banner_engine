
/*global module:false*/
module.exports = function(grunt) {

    // Get the current UTC date
    var currentDate = new Date();
    var yyyy = currentDate.getUTCFullYear();
    var momo = String(currentDate.getUTCMonth() + 1).length !== 1 ? currentDate.getUTCMonth() + 1 : '0' + currentDate.getUTCMonth() + 1;
    var dd = String(currentDate.getUTCDate()).length !== 1 ? currentDate.getUTCDate() : '0' + currentDate.getUTCDate();
    var hh = String(currentDate.getUTCHours()).length !== 1 ? currentDate.getUTCHours() : '0' + currentDate.getUTCHours();
    var mm = String(currentDate.getUTCMinutes()).length !== 1 ? currentDate.getUTCMinutes() : '0' + currentDate.getUTCMinutes();
    var ss = String(currentDate.getUTCSeconds()).length !== 1 ? currentDate.getUTCSeconds() : '0' + currentDate.getUTCSeconds();
    currentDate = yyyy + '-' + momo + '-' + dd + '_' + hh + mm + ss;


    // Process function for when we want to deploy a dist
    const process = (content, srcpath) => {
        if (srcpath.indexOf('index.html') >= 0)
            return content.replace(/<script.+<\/script>/g, '').replace('margin: 0px;', 'margin: 10px;');
        else if (srcpath.indexOf('.html') >= 0)
            return content.replace('src="./js/code_concat.js"', 'src="./js/code.min.js"');
        else return content;
    }


    // Check which sizes do we need
    const bannerSizes = {
        w160h600: null,
        w300h250: null,
        w300h600: null,
        w320h50: null,
        w336h280: null,
        w728h90: null,
        w970h250: null
    };

    const createSetup = (size) => {
        const bannerSize = size.slice(1, size.indexOf('h')) + 'x' + size.slice(size.indexOf('h') + 1);
        return {
            expand: true,
            src: [
                'css/' + bannerSize + '.css',
                'fonts/**/*',
                'img/*.png',
                'img/*.jpg',
                'img/*.svg',
                'img/' + bannerSize + '/*',
                'js/code.min.js',
                bannerSize + '.html'
            ],
            dest: __dirname + '_dist/' + currentDate + '/' + bannerSize,
            options: {
                process: (content, srcpath) => process(content, srcpath),
                noProcess: '**/*.{jpg,jpeg,png,gif,svg,psd,ttf,eot,woff,woff2}'
            }
        }
    }

    const bannerSizeKeys = Object.keys(bannerSizes);
    for (let i in bannerSizeKeys) {
        const key = bannerSizeKeys[i];
        bannerSizes[key] = createSetup(key);
    }


    // Project configuration.
    grunt.initConfig({

        connect: {
            banners: {
                options: {
                    hostname: 'localhost',
                    livereload: true,
                    open: true,
                    port: 9000,
                    base: {
                        path: '.',
                        options: {
                            index: 'index.html',
                            maxAge: 300000
                        }
                    }
                }
            }
        },

        watch: {
            livereload: {
                options: { livereload: true },
                files: [/*'js/code_concat.js',*/ 'css/*']
            },
            js: {
                files: [
                    'js/code.js'
                ],
                tasks: ['concat']
            },
            js_concat: {
                files: [
                    'js/code_concat.js'
                ],
                tasks: ['uglify']
            },
            css: {
                files: 'sass/*.scss',
                tasks: ['sass']
            }
        },

        concat: {
            js: {
                src: [
                    'js/webfont.js',
                    'js/code.js'
                ],
                dest: 'js/code_concat.js'
            }
        },

        uglify: {
            dist: {
                src: '<%= concat.js.dest %>',
                dest: 'js/code.min.js'
            }
        },

        sass: {
            dist: {
                options: {
                    style: 'compressed',
                    compass: true,
                    update: true
                },
                files: [{
                    expand: true,
                    cwd: 'sass/',
                    src: ['*.scss'],
                    dest: 'css/',
                    ext: '.css'
                }]
            }
        },

        copy: {
            main: {
                expand: true,
                src: [
                    'css/*.css',
                    'fonts/**/*',
                    'img/*',
                    'js/code.min.js',
                    '*.html'
                ],
                dest: __dirname + '_dist/' + currentDate,
                options: {
                    process: (content, srcpath) => process(content, srcpath),
                    noProcess: '**/*.{jpg,jpeg,png,gif,svg,psd,ttf,eot,woff,woff2}'
                }
            },
            w160h600: bannerSizes.w160h600,
            w300h600: bannerSizes.w300h600,
            w300h250: bannerSizes.w300h250,
            w336h280: bannerSizes.w336h280,
            w320h50: bannerSizes.w320h50,
            w728h90: bannerSizes.w728h90,
            w970h250: bannerSizes.w970h250,
        }
    });


    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-copy');


    // Default task.
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('serve', ['connect', 'watch']);
    grunt.registerTask('dist', ['copy:main']);
    grunt.registerTask('dist:sizes', () => { // Execute all copy jobs except the main one
        delete grunt.config.data.copy.main;
        grunt.task.run('copy');
    });
};
