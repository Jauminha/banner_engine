
/****************************
********  PRE-SETUP  ********
*****************************/

// A helper to clear the code
id = el => document.getElementById(el);


// The version of IE, if its not IE, == false
const ie = ( () => {
	const ua = window.navigator.userAgent;

	// IE 10 or older => return version number
	const msie = ua.indexOf('MSIE ');
	if (msie > 0) return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);

	// IE 11 => return version number
	const trident = ua.indexOf('Trident/');
	if (trident > 0) {
		const rv = ua.indexOf('rv:');
		return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
	}

	// Edge (IE 12+) => return version number
	const edge = ua.indexOf('Edge/');
	if (edge > 0) return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);

	// other browser
	return false;
})();
// add details to console
//console.log(window.navigator.userAgent);


// Define ad size
const adSize = (window.location.pathname.split('/').pop()).split('.html')[0];
const adWidth = adSize.split('x')[0];
const adHeight = adSize.split('x')[1];


// Setup the proper protocol
const locationProtocol = document.location.protocol == 'file:' ? 'http' : document.location.protocol;



/****************************
******  CONFIGURATION  ******
*****************************/

// Getting all the parameteres in the URL, also for when clicktag its not the only url parameter
const parameters = window.location.search.split('?').slice(1).join('?').toString();


// Click to prepend to the clickUrl, must be placed at the end of the parameters
let clickTag = '';
if (parameters.indexOf('clickTag') != -1) clickTag = parameters.split('clickTag=')[1].toString();


// The default language for the feed in case we can't find the correct one. It must exist
let defaultLanguage = 'en';
if (parameters.indexOf('defaultLanguage') != -1) {
	defaultLanguage = parameters.split('defaultLanguage=')[1].toString();
	if (defaultLanguage.indexOf('&') != -1) defaultLanguage = defaultLanguage.split('&')[0].toString();
}


// Structural and animation vars
const productFeedName = ''; // If we have an external feed
let feedLoaded; // This will be initialized to true or false
let currentFrame = 1; // The current frame the animation will start with
let animationSpeed = 1; // What's the speed of the animation?
let animationRepeats = 0; // How many times the animation has to repeat?
const numProducts = 1; // It has to be minimum 1
const numFrames = numProducts + 1; // <--------------------------------⋋
// If you want to add or remove presentation frames increase/decrease THIS number, then edit @jsonp function accordingly

// Special vars for this project
// ...



/****************************
******** FONT LOADER ********
*****************************/

WebFont.load({
	custom: {
		families: ['minitype_v2_regularbold']
	},
	active: () => {
		// When font is loaded resize texts
		const allTexts = document.querySelectorAll('.text');
		allTexts.forEach(text => fitText(text, 8, 90));

		/****************************
		*** GOOGLE STUDIO ENABLER ***
		*****************************/
		enablerInitialized = () => {
			// In App ads are rendered offscreen so animation should wait for the visible event.
			if (!Enabler.isVisible()) Enabler.addEventListener(studio.events.StudioEvent.VISIBLE, adVisible);
			else adVisible();
		}
		adVisible = () => {
			/****************************
			*** RELEASE THE KRAKEEEN! ***
			*****************************/
			initAnimation();
		}

		if (!Enabler.isInitialized()) Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitialized);
		else enablerInitialized();
	},
});



/****************************
*********  LANGUAGE  ********
*****************************/

defineLanguage = () => {

	setLanguage = '';
	if (parameters.indexOf('enforceLanguage') != -1) {
		// We enforce in case we identify the user's preferred language with piggyback pixel tracking
		setLanguage = parameters.split('enforceLanguage=')[1].toString();
		if (setLanguage.indexOf('&') != -1) setLanguage = setLanguage.split('&')[0].toString();
		defineJsonpContent(setLanguage);
	}
	else getBrowserLanguage();
}


getBrowserLanguage = () => {

	const browserLanguage = navigator.language || navigator.userLanguage;
	// If browser language contains - (e.g. 'en-US'), split it on the -
	if (browserLanguage.indexOf('-') != -1) setLanguage = browserLanguage.split('-')[0];
	else if (browserLanguage != undefined) setLanguage = browserLanguage;
	else setLanguage = defaultLanguage;
	defineJsonpContent(setLanguage);
}


defineJsonpContent = language => {

	if (productFeedName != '') { // If we have a feed to call
		// Create the call url for the feed
		const productFeedLocation = locationProtocol + '...' + language; //Depends on DSP, Agency, etc
		// Retrieve JSONP and put in script tag in the HTML head. We may have to parse the return if XML
		const jsonpScriptTag = document.createElement('script');
		jsonpScriptTag.src = productFeedLocation;
		jsonpScriptTag.onload = () => {
			// The JSONP must have a call to the @jsonp function or else we have to adapt
			if (feedLoaded == undefined) backupFeed();
		};
		document.getElementsByTagName('head')[0].appendChild(jsonpScriptTag);
	}
	else backupFeed();
}



/****************************
******** TEXT RESIZE ********
*****************************/

resizeLoop = (min, max, testTag, checkSize) => {
	let fontSize = min;

	for (let i = 0; i < 30; i++) { // Loop for a max of 30 steps
		testTag.style.fontSize = fontSize + 'px';
		if (checkSize(testTag)) { // The son is bigger than the parent, we found a new max
			max = fontSize;
			fontSize = (fontSize + min) / 2;
		}
		else {
			if (max == 0) { // Start by growing exponentially
				min = fontSize;
				fontSize *= 2;
			}
			else {
				// If we're within 1px of max anyway, call it a day
				if (max - fontSize < 0.1) break;

				// If we've seen a max, move half way to it
				min = fontSize;
				fontSize = (fontSize + max) / 2;
			}
		}
	}

	return fontSize;
}


fitText = (tag, min, max) => {

	tag.style.width = Math.floor(parseInt(tag.offsetWidth)) + 'px';

    // Clone original tag and append to the same place so we keep its original styles, especially font
	const testTag = tag.cloneNode(true);
	testTag.style.position = 'absolute';
	testTag.style.margin = 0;
	testTag.style.left = 0;
	testTag.style.top = 0;
	testTag.style.right = 'auto';
	testTag.style.bottom = 'auto';
	testTag.style.listStyleType = 'none';
	testTag.style.visibility = 'hidden';

	tag.appendChild(testTag);

	const tagInterval = setInterval( () => {
		// Reset some DOM values for the parent
		tag.style.display = 'none';
		tag.style.display = 'block';
		// If the DOM has rendered the parent properly
	    if ((tag.offsetWidth + 1) >= testTag.offsetWidth && (tag.offsetHeight + 1) >= testTag.offsetHeight) {

		    testTag.style.width = 'auto';
		    testTag.style.height = 'auto';

		    //const fontSize;
		    const fontSize = resizeLoop(min, max, testTag, (t) => {
		        return t.offsetWidth > tag.offsetWidth || t.offsetHeight > tag.offsetHeight;
		    });

		    testTag.parentNode.removeChild(testTag);
		    tag.style.fontSize = Math.floor(fontSize) + 'px';

		    // Special treatments
		    afterResize(tag, Math.floor(fontSize));

		    clearInterval(tagInterval);
	    }
	}, 25);
};


let textsResized = false;
afterResize = (el, fontSize) => {

	// Do we still support ie8?
	const style = (ie <= 8 && ie != false) ? el.currentStyle : getComputedStyle(el);

	// We need to get the DOM some time to render the fonts properly, so we loop for 500ms every XXms
	setTimeout( () => textsResized = true, 500 );
	if (!textsResized) setTimeout( () => fitText(el, 8, 90), 100 );
	else {
		// Once we are finished we want to align the texts vertically. If you want to exclude some texts just create an if statement
		if (adSize == '320x50' || adSize == '728x90' || adSize == '970x250') {
			const lastHeight = el.offsetHeight;
			el.style.height = 'auto';
			el.style.paddingTop = (parseInt(style.paddingTop) + (lastHeight - el.offsetHeight) / 2) + 'px';
			el.style.height = lastHeight + 'px';
		}

	}
}



/****************************
********* ANIMATION *********
*****************************/

initAnimation = () => {

	// Setup animation config through GreenSock Timeline
	const tl = new TimelineMax({
		repeat: animationRepeats,
		onRepeat: () => {
			animationRepeats--;
			currentFrame = 1;
			id('frame_' + currentFrame).style.zIndex = 9;
			// Special treatments when repeating the animation go here
		}
	});

	// Speed of the animation
	tl.timeScale(animationSpeed);

	// Place the current frame on top
	id('frame_' + currentFrame).style.zIndex = 9;

	// Show the banner for the firs time
	document.body.style = 'opacity: 1;';


	// Setup the functions needed for the animation
	onPresentationFrame = n => {
		// Declare local vars for anim

		// Animate
		tl.fromTo(id('presentation_' + n), 1, {opacity: 0}, {opacity: 1})
		  .to(id('presentation_' + n), 1, {opacity: 0}, '+=2');
	}


	onProductFrame = n => {
		// Declare local vars for anim

		// Animate
		tl.fromTo(id('title_' + n), 1, { opacity: 0 }, { opacity: 1, onComplete: () => {
			if (animationRepeats == 0 && currentFrame == numFrames) stop();
		}});
	}


	stop = () => {
		tl.stop();
	}


	for (let i = currentFrame; i <= numFrames; i++) {
		// Create an intro. Show first the logo and/or other elements
		if (i == 1) tl.staggerFromTo([id('logo')], 1, {opacity: 0}, {opacity: 1}, 0.5, '+=0.75', () => {});

		// On all frames except the last one
		if (i < numFrames) {
			// Show the current frame
			tl.fromTo(id('frame_' + i), 1, {opacity: 0}, {opacity: 1}, '-=0.25');

			// On presentation frame
			if (numFrames > numProducts && i == 1)
				 onPresentationFrame(i);
			else onProductFrame(i);

			// Hide the current frame, prepare for next
			tl.to(id('frame_' + i), 1, {opacity: 0, onComplete: () => {
				id('frame_' + currentFrame).style.zIndex = 1;
				currentFrame++;
				id('frame_' + currentFrame).style.zIndex = 9;
			}}, '+=0');
		}

		// On the last frame
		if (i == numFrames) {
			tl.fromTo(id('frame_' + i), 1, {opacity: 0}, {opacity: 1});

			// On product frame
			onProductFrame(i);

			// Add this label wherever you want to timeline previewer to stop
			if (animationRepeats == 0) tl.addLabel('end');

			// Reset the banner for the repeat
			tl.to([id('frame_' + i), id('logo')], 1, {opacity: 0, onComplete: () => {
				id('frame_' + currentFrame).style.zIndex = 1;
			}},'+=3.5');
		}

		// Extra functions for the animation go here (create more timelines inside if needed)
	}

	// TimeLineMax listeners for preview purposes only
	animControls(tl);
}


/****************************
**** ANIMATION CONTROLS *****
*****************************/

const animControls = (timeLine) => {
	// For preview purposes only
	window.onmessage = function (e) {
		if (e.data.action == 'animGoTo') {
			let totalDuration = timeLine.getLabelTime('end');
			timeLine.seek(totalDuration * e.data.value);
			timeLine.pause();
		}
		else if (e.data.action == 'animPlay') {
			let totalDuration = timeLine.getLabelTime('end');
			timeLine.seek(totalDuration * e.data.value);
			timeLine.play();
		}
	};
}



/****************************
******** CONSTRUCTOR ********
*****************************/

// When the call to the feed has been successfull or the backup is used this will run
jsonp = jsonObject => {

	if (feedLoaded == undefined) {
		feedLoaded = true;
		console.log('Feed loaded properly');
	}

	// Get the general info from the feed/backup
	let general = jsonObject.general[setLanguage];
	if (general == undefined) { // If the language doesn't exist go back to the default
		general = jsonObject.general[defaultLanguage];
		setLanguage = defaultLanguage;
	}

	// Define wrapper
	const wrapper = id('wrapper');

	// Define logo
	const logo = id('logo');
	logo.src = './img/logo.png';


	// Define frames
	for (let key = 1; key <= numFrames; key++) {

		const frame = document.createElement('div');
		frame.id = 'frame_' + key;
		frame.className = 'frame';
		wrapper.appendChild(frame);

		// Presentation frames
		if (numFrames > numProducts && key <= (numFrames - numProducts)) {
			//Create and append elements to the frame
			frame.innerHTML = `
				<div id="presentation_${key}" class="presentation text">${general.pres_text}</div>
			`;
		}
		// Product frames
		else {
			// Get the info from the actual product
			const productToSelect = key - (numFrames - numProducts);
			let product = jsonObject.product[productToSelect].content[setLanguage];

			// Create the elements of the poduct frame
			frame.innerHTML = `
				<div id="title_${key}" class="title text">${product.title}</div>
				<div id="cta_${key}" class="cta text">${general.cta_text}</div>
			`;

			// Append clicks to the frames and logo
			let finalClick = clickTag + product.clickurl;
			if (numProducts == 1) wrapper.setAttribute('onclick', 'javascript:window.open("' + finalClick + '", "_blank");');
			else {
				if (productToSelect == 1) logo.setAttribute('onclick', 'javascript:window.open("' + finalClick + '", "_blank");');
				if (numFrames > numProducts) id('frame_1').setAttribute('onclick', 'javascript:window.open("' + finalClick + '", "_blank");');
				frame.setAttribute('onclick', 'javascript:window.open("' + finalClick + '", "_blank");');
			}
		}
	}
}


// When the call to the feed has NOT been successfull this will run.
// The backup and the feed structure must be the same unless we create a different structure for the frames in @jsonp
backupFeed = () => {

	feedLoaded = false;
	console.log('Feed failed to load, backup used');

	// We can call the jsonp(jsonObject) function with a backup json or do something else
	const jsonObject = {
		"general": {
			"en": {
				"pres_text": "The new Products!",
				"cta_text": "Explore Now"
			}
		},
		"product": {
			"1": {
				"content": {
					"en": {
						"clickurl": "https://example.com",
						"title": "Don't miss out!"
					}
				},
				"id": "brand-name_backup_1"
			}
		}
	};

	jsonp(jsonObject);
}


/****************************
*** CONSTRUCT THE BANNER ****
*****************************/

defineLanguage();
