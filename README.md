# IAB Banner set engine
![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)

## Example
Here's one example that uses this code (it's actually a previous version): https://jaumecalm.com/iframes/mini-example

## Contents
- [Installation](#installation)
- [Structure](#structure)
- [Usage](#usage)
    - [Code.js](#code.js)
        - [Configuration](#configuration)
        - [Font loader](#font-loader)
        - [Animation](#animation)
        - [Constructor](#constructor)
    - [_global_styles.scss](#_global_styles.scss)
    - [adSize.scss](#adSize.scss)


## Installation
Download, unzip the package and run `npm install`.

## Structure
The project is structured as follow:

`aid`: Contains js files that help with the previsualization of the banners.

`css`: Exported css and maps (for developer tools that support them ) from the scss files.

`fonts`: The custom fonts used for this set.

`img`: Any images used for the banners.

`js`: Contains the js files used for the banners. Only `code.js` has to be edited.

`sass`: Contains all scss files. One for each banner size.

`index.html`: Preview of all banners toghether, uses the files in the `aid` dir. Contains a slider to manipulate the timeline of the animation. The play button next to it will play the animation from the point where the slider is located.

`width*height.html`: Contains the minimun html markup for the banners to start. One for each ad size.

Just add or remove sizes depending on the requirements of your new set. Remember to create the scss files as well.

## Usage
After all the packages have been installed you can use the following commands in your terminal:

`grunt`: Starts watching for changes in `js/code.js` and any `sass/*.scss` file.

`grunt serve`: Same as `grunt`, but also starting a http server which opens a new browser tab to the `index.html` file.

`grunt dist`: Exports the project with all sources in an external folder ready for staging/production. A timestamp is used to mark the current export.

`grunt dist:sizes`: Same as `grunt dist`, but separating the exports by ad size in diferent folders. Note that for this to work properly, all assets specific to an ad size in the `img` folder, must be placed inside a specific folder for that ad size:
```
img
├── logo.png
├── product1.png
├── 160x600
│   ├── product2.png
│   └── bg_160x600.png
├── 300x600
│   ├── product2.png
│   └── bg_300x600.png
...
```

If you just want to make some changes in the scss files you can also use the typical Sass commands with `compass watch`.

See the `Gruntfile.js` & `config.rb` files for the actual configs. Feel free to make any changes to accomodate your needs.

### Code.js
The code is self explanatory and contains plenty of comments to understand all functionalities, but let's delve into some of them:

#### Configuration
```js
// Getting all the parameteres in the URL, also for when clicktag its not the only url parameter
const parameters = window.location.search.split('?').slice(1).join('?').toString();

// Click to prepend to the clickUrl
let clickTag = '';
if (parameters.indexOf('clickTag') != -1) {
	clickTag = parameters.split('clickTag=')[1].toString();
	if (clickTag.indexOf('&') != -1) clickTag = clickTag.split('&')[0].toString();
}

// The default language for the feed in case we can't find the correct one. It must exist
let defaultLanguage = 'en';
if (parameters.indexOf('defaultLanguage') != -1) {
	defaultLanguage = parameters.split('defaultLanguage=')[1].toString();
	if (defaultLanguage.indexOf('&') != -1) defaultLanguage = defaultLanguage.split('&')[0].toString();
}
```
Here we can see how we handle url parameters, just duplicate one of them and change the name of the variable to the name of the new parameter you wan to parse.

#### Font loader
We are using the [webfont.js](https://github.com/typekit/webfontloader) loader to ensure our custom fonts load before we resize the texts and initiate the animation. Make sure to include them here:
```js
WebFont.load({
	custom: {
		families: ['minitype_v2_regularbold']
	},
	active: () => {
		// When font is loaded resize texts
		const allTexts = document.querySelectorAll('.text');
		allTexts.forEach(text => fitText(text, 8, 90));
		// Start the animation
		initAnimation();
	},
});
```

#### Animation
We are using TimelineMax from [GSAP](https://greensock.com/gsap).
The animation code for all sizes is shared, in case you need fine tunning check for the comments that say `Declare local vars for anim` and create an object for each size. Then use it in the tweens selecting the size by using the `adSize` variable. Here's an example:
```js
const imageTrans = {
    '300x250': {
        left: 0, height: '100%'
    },
    '160x600': {
        left: '-335%', width: '436%'
    },
    '728x90': {
        left: 0, height: '230%', bottom: 0
    },
    '320x50': {
        left: 0, height: '184%', bottom: 0
    }
}
```

If you even need more fine tunning you can then create an if statement and select specific adSizes like so:
```js
tl.from(background, 3, {
    left: imageTrans[adSize]['left'],
    ease: Power1.easeInOut }, '+=0.25'
);
if (adSize != '160x600') tl.from(background, 2.5, {
    height: imageTrans[adSize]['height'],
    ease: Power2.easeInOut },
'-=2');
else tl.from(background, 2.5, {
    width: imageTrans[adSize]['height'],
    ease: Power2.easeInOut },
'-=2');
```

Remember there's also the `ie` variable that states `The version of IE, if its not IE, == false`. You can use that as well if you know of certain cross-browser compatibility issues like css `mix-blend-mode: color-dodge;` which doesn't work on animated elements in IE.


#### Constructor
In this section we build the banner's structure. To do so we can populate the banner in two ways. One is with an external feed which will have to be loaded in the `defineJsonpContent` function.
In case there's no feed or the return/callback of such is undefined, the `backupFeed` function will be used.

Both (external feed & backup) need to have the same structure so the callback `jsonp` function can process their data. Since the feed may contain more products than we want and we also may want to show more than only product frames, we can play with the `numFrames` & `numProduct` variables.

### _global_styles.scss
In this file we will apply all the styles that are common to all banner sizes. Mostly you can consider this file as the "style/brand guide".

### adSize.scss
These files will contain the final positioning of all the elements in the banner. This approach will let you create the final look and feel so then you can work on the animation by mostly using 'from' tweens and not having to worry about the repeats.
